# Codemera's Homepage Test Project

This repository contains the files for Carlos Mateo's Codemera's Homepage test project. A mobile first homepage proposal for Codemera's website.


## About the project
---
This project was developed basically using HTML, CSS and javascript, using the following technologies:
- [SASS] CSS Pre-processor: was used due to it's advantages and relative fast and easy pace to generate CSS.
- [Bootstrap]: a framework for developing responsive, mobile first projects for the web. Used with SASS in order to separate presentation and content (basically CSS and HTML). Only bootstrap grid system and some sass variables and mixins were used.
- [Bower]: a dependencies package manager, used so there's no need to commit dependencies to the version control system, making the project relatively lighter when cloning or downloading it.
- [jQuery]: a javascript library that makes document traversal and manipulation, event handling and animation easy to the project. Also because of other great libraries that need jQuery in order to run properly, like [Viewport Checker] a library used to make easy animate every section of the project as they are scrolled.


## Installation
---
To install or run this project properly, after cloning or downloading it, all dependencies should be installed using bower. Using the console, go to the project's directory and run the following code in the terminal:

```sh
$ bower install
```
A folder named "bower_components" containing most of the project dependencies would appears in the project's root directory.


## Compatibility list
---
Currently the project has been tested and works correctly in the following browsers:
- **Google Chrome** version 43
- **Mozilla Firefox** version 38.0.1
- **Apple Safari** version 8.0.6
- **Opera** version 12.15
- **Internet Explorer** version 10




[Viewport Checker]: https://github.com/dirkgroenen/jQuery-viewport-checker
[Bower]: http://bower.io/
[SASS]: http://sass-lang.com/
[jQuery]: http://jquery.com/
[Bootstrap]: http://getbootstrap.com/