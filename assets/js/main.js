//Variables
var headerOffsetY, mainTagOffsetY = 0;

$(document).ready(function() {

	//*********************************************
	//	Mobile navigation button funcionality
	//*********************************************
	$("#main-navigation-mobile").on("click", function(e) {
		e.preventDefault();

		if( $("#main-navigation ul").hasClass("active") ) {
			$("#main-navigation ul").removeClass("active");
		} else {
			$("#main-navigation ul").addClass("active");
		}
	});


	//*********************************************
	//	Sticky menu background on scroll
	//*********************************************
	$(window).on("scroll", function(e) {
		headerOffsetY = $("#main-header").offset().top + 50;
		mainTagOffsetY = $("main#home").offset().top;
		
		if( headerOffsetY > mainTagOffsetY ) {
			$("#main-header").addClass("sticky-menu");
		} else {
			$("#main-header").removeClass("sticky-menu");
		}
	});

});



//*********************************************
//	Animations using Viewport Checker 
//*********************************************
$(window).load(function() {
	//Jumbotron
    $('#jumbotron').viewportChecker({
    	classToAdd: 'visible animated fadeIn',
        offset: 0
    });


    //Animations for "Who we are" section
    $('#who-we-are').viewportChecker({
        classToAdd: 'visible animated fadeInUp',
        callbackFunction: whoWeAreTitle(),
        offset: 0
    });

	    function whoWeAreTitle() {
		    setTimeout(function() {
		        $('#who-we-are h2').viewportChecker({
		            callbackFunction: whoWeAreText(),
		            classToAdd: 'visible animated fadeInUp',
		            offset: 0
		        });
		    }, 300);
		}

		function whoWeAreText() {
		    setTimeout(function() {
		        $('#who-we-are .intro').viewportChecker({
		            callbackFunction: whoWeAreButton(),
		            classToAdd: 'visible animated fadeInUp',
		            offset: 0
		        });
		    }, 300);
		}

		function whoWeAreButton() {
		    setTimeout(function() {
		        $('#who-we-are .buttons').viewportChecker({
		            classToAdd: 'visible animated fadeInUp',
		            offset: 0
		        });
		    }, 300);
		}


    //What we do
    $('#what-we-do').viewportChecker({
        classToAdd: 'visible animated fadeInUp',
        callbackFunction: whatWeDoTitle(),
        offset: 0
    });

    	function whatWeDoTitle() {
		    setTimeout(function() {
		        $('#what-we-do h2').viewportChecker({
		            callbackFunction: whatWeDoText(),
		            classToAdd: 'visible animated fadeInUp',
		            offset: 0
		        });
		    }, 300);
		}

		function whatWeDoText() {
		    setTimeout(function() {
		        $('#what-we-do .intro').viewportChecker({
		        	callbackFunction: whatWeDoTextBlocks(),
		            classToAdd: 'visible animated fadeInUp',
		            offset: 0
		        });
		    }, 300);
		}


		function whatWeDoTextBlocks() {
		    setTimeout(function() {
		        $('#what-we-do .more div').viewportChecker({
		            classToAdd: 'visible animated fadeInUp',
		            offset: 0
		        });
		    }, 300);
		}



    //Selected work
    $('#selected-work').viewportChecker({
        classToAdd: 'visible animated fadeIn',
        callbackFunction: selectedWorkTitle(),
        offset: 0
    });

    	function selectedWorkTitle() {
		    setTimeout(function() {
		        $('#selected-work h2').viewportChecker({
		            callbackFunction: selectedWorkText(),
		            classToAdd: 'visible animated fadeInUp',
		            offset: 0
		        });
		    }, 300);
		}

		function selectedWorkText() {
		    setTimeout(function() {
		        $('#selected-work .intro').viewportChecker({
		        	callbackFunction: selectedWorkClients(),
		            classToAdd: 'visible animated fadeInUp',
		            offset: 0
		        });
		    }, 300);
		}

		function selectedWorkClients() {
		    setTimeout(function() {
		        $('#selected-work ul li').viewportChecker({
		        	callbackFunction: selectedWorkButton(),
		            classToAdd: 'visible animated fadeInUp',
		            offset: 0
		        });
		    }, 300);
		}

		function selectedWorkButton() {
		    setTimeout(function() {
		        $('#selected-work .buttons').viewportChecker({
		            classToAdd: 'visible animated fadeInUp',
		            offset: 0
		        });
		    }, 300);
		}
});